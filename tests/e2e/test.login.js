const { Builder, Browser, By } = require("selenium-webdriver");
const {Options} = require('selenium-webdriver/chrome')
const assert = require('assert');
const request = require('supertest');
const server = require("../../index");
const agent = request(server);

describe('E2E Test: Login',  async function () {
    this.timeout(10000);
    let testValidUser = {
        username: 'validuser',
        password: 'strongpassword'
    }

    let testInvalidUser = {
        username: 'invaliduser',
        password: 'wrongpassword'
    }

    let driver;
    let login_url;

    before(async function() {
        console.log('[Selenium Webdriver] Preparing...');

        let options = new Options();
        options.addArguments('--disable-dev-shm-usage');
        options.addArguments('--no-sandbox');
        options.addArguments('--headless');

        try {
            if (process.env.SELENIUM_SERVER == 'remote') {
                console.log('[Selenium Webdriver] Using remote Selenium Hub');
                driver = await new Builder()
                    .forBrowser(Browser.CHROME)
                    .usingServer(process.env.SELENIUM_REMOTE_URL)
                    .build();
                login_url = 'http://dev-app-nodejs:3000/login';

            } else {
                console.log("[Selenium Webdriver] Using host brower");
                driver = await new Builder()
                    .forBrowser('chrome')
                    .setChromeOptions(options)
                    .build();
                login_url = 'http://localhost:3000/login';
            }
            console.log('[Selenium Webdriver] Get ready!');
        } catch (e) {
            console.error('[Selenium Webdriver] Failed to prepare webdriver: ', e);
            throw(e.message);
        }
    })

    it('[驗證身份] 合法使用者，須看到歡迎訊息', async function() {
        try {
            await driver.get(login_url);

            // input username
            await driver
                .findElement(By.name('username'))
                .sendKeys(testValidUser.username);

            // input password
            await driver
                .findElement(By.name('password'))
                .sendKeys(testValidUser.password);

            // submit user input
            await driver
                .findElement(By.name('submit'))
                .click();

            let helloMsg = await driver
                .findElement(By.name('user_msg'))
                .getText();

            console.log('helloMsg:', helloMsg);

            assert.equal(helloMsg, `Hello, ${testValidUser.username}! Welcome to our website.`);
          } catch (e) {
            console.error('err:', e);
            throw (e.message);
          }
    });

    it('[驗證身份] 非法使用者，須看到資訊錯誤提醒', async function() {
        try {
            await driver.get(login_url);

            // input username
            await driver
                .findElement(By.name('username'))
                .sendKeys(testInvalidUser.username);

            // input password
            await driver
                .findElement(By.name('password'))
                .sendKeys(testInvalidUser.password);

            // submit
            await driver
                .findElement(By.name('submit'))
                .click();

            let errorMsg = await driver
                .findElement(By.name('error_msg'))
                .getText();
            console.log('errorMsg:', errorMsg);
            assert.equal(errorMsg, 'Wrong username or password. Try again!');
          } catch (e) {
            console.error('err:', e);
            throw (e.message);
          }
    });

    after(async function(){
        console.log('[Selenium Webdriver] Closing...');
        try {
            await driver.quit();
            console.log('[Selenium Webdriver] Closed!');
        } catch (e) {
            console.log('[Selenium Webdriver] Failed to close: ', e);
            throw (e.message);
        }
    })
})